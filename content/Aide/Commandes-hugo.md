---
title: "Commandes Hugo"
date:  2018-01-02T15:06:47+01:00
weight: 5
---

**Note préliminaire :** les commandes sont à exécuter à la racine du dépôt (pas depuis le dossier `content` sinon cela ne fonctionne pas et génère une erreur)

## Créer un chapitre

Pour créer le chapitre *Groupes de Travail*, faire depuis la racine du dépôt :

```
hugo new --kind chapter "Groupes de travail/_index.md"
```

* Référence : [Créer votre première page chapitre](https://learn.netlify.com/fr/basics/installation/#créer-votre-première-page-chapitre)

## Créer une page

Pour créer la page *Commandes Hugo* dans le chapitre *Aide*, faire depuis la **racine du dépôt** :

```
hugo new Aide/Commandes-hugo.md
```

ou encore

```
hugo new "Groupes de travail/Communication/Ecrire-une-actualite.md"
```

* Remarque : Hugo créé automatiquement le titre de la page en remplaçant les tirets par des espaces (`Commandes-hugo.md` a pour titre *Commandes Hugo*)
* Référence : [Créer votre première page](https://learn.netlify.com/fr/basics/installation/#créer-votre-première-page-chapitre)

## Lancer le serveur Hugo localement

1. Se placer à la racine du dossier `Guide-CDMEB`
1. Lancer `hugo server --forceSyncStatic --watch --i18n-warnings --disableFastRender`
1.Consulter le site sur [http://localhost:1313/](http://localhost:1313/)


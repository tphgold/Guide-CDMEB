---
title: "Pourquoi un Guide"
date:  2018-01-02T15:21:33+01:00
weight: 5
---

Le Guide CDMEB décrit les processus du CDMEB. Les raisons pour lesquelles nos processus sont décrits dans un guide sont les suivantes:

* La lecture est beaucoup plus rapide que l'écoute.
* La lecture est asynchrone, vous n'avez pas besoin d'interrompre quelqu'un ou d'attendre qu'il soit disponible.
* Le recrutement est plus facile si les gens peuvent voir ce que nous défendons et comment nous opérons.
* La rétention est préférable si les gens savent dans quoi ils s'embarquent avant d'y adhérer.
* L'intégration est plus facile si vous trouvez toutes les informations pertinentes en toutes lettres.
* Le travail d'équipe est plus facile si vous pouvez lire comment les autres parties de l'organisation fonctionnent.
* Il est plus facile de discuter des changements si vous pouvez lire le processus actuel.
* Communiquer le changement est plus facile si vous pouvez simplement pointer du doigt la différence.
* Tout le monde peut y contribuer en proposant un changement via une demande de fusion (*merge request*).

Il s'inspire du [Handbook Gitlab](https://about.gitlab.com/handbook/).

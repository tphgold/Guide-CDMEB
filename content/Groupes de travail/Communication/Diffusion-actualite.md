---
title: "Diffusion d'une actualité"
weight: 5
---

Ce Document liste les étapes à faire pour diffuser une actualité rédigée sur le site WWW.moniteurs.glenans.asso.fr. Nous trouvons ici :

1. La liste des "lieux" sur lesquels partager l'actualité
1. Pour chacun d'eux les étapes à ne pas oublier

## Twitter

Actions post-rédaction :

* faire un Like
* Ajouter l'URL du tweet dans la page de la news (éventuellement en commentaire)

## Facebook

Actions post-rédaction :

* faire un J'Aime (en tant qu'individu, et aussi en tant que Glénans_CDMEB)
* Ajouter l'URL du message Facebook dans la page de la news (éventuellement en commentaire)
* Partager sur les autres pages / groupes FB (à faire en tant que Glénans_CDMEB)
    * Cliquer sur `Partager` puis `Partager dans un groupe`, taper "Glénans", sélectionner **Les Glenans, école de voile, école de mer, école de vie**, valider
    * Cliquer sur `Partager` puis `Partager dans un groupe`, taper "Glénans", sélectionner **Les Glenans - Comité de secteur Arz**, valider
    * Cliquer sur `Partager` puis `Partager dans un groupe`, taper "Glénans", sélectionner **Les Glenans - Comité de secteur Paimpol**, valider
    * Cliquer sur `Partager` puis `Partager dans un groupe`, taper "Glénans", sélectionner **Les Glenans - Comité de secteur Archipel**, valider
    * Cliquer sur `Partager` puis `Partager sur le journal d'un ami`, taper "Glénans", sélectionner **Glénans Bonifacio**, valider
    * Cliquer sur `Partager` puis `Partager sur le journal d'un ami`, taper "Glénans", sélectionner **Glénans CsMed Marseillan Bonifacio**, valider

Notes:

* pas trouvé comment poster sur la [page Les Glénans - Centre de formation](https://www.facebook.com/glenansformation/)
* pas trouvé comment poster sur la [page Les Glénans Marseillan](https://www.facebook.com/Les-Gl%C3%A9nans-Marseillan-179068912166395/)

## LinkedIn

@@@TODO

## Listes

Envoyer un courriel avec le lien vers la news sur notre site aux listes : 

* `membres`
* `diffusion-sympathisants`
*  `documentalistes` (Matthieu)

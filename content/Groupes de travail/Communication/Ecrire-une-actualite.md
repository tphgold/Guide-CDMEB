---
title: "Écrire une actualité"
date:  2018-02-12T18:16:19+01:00
weight: 5
---

## Pré-requis

* Avoir un compte sur [WWW.moniteurs.glenans.asso.fr](WWW.moniteurs.glenans.asso.fr)

## Recommandations globales

Ici : les grandes lignes qui sont ensuite détaillées plus bas

* Chaque article devrait avoir une image d'illustration.
* Il n'y a pas de limite à la longueur du texte (mais si c'est vraiment très long, autant faire plusieurs articles).

## Recommandations sur les photos : choisir une photo

L’image permet d’ajouter un visuel agréable, elle est optionnelle mais donne un plus non-négligeable à votre actualité ! Recommandations pour les image:

* éviter de copier/coller une image prise comme ça sur l’internet (respect du droit d’auteur) ;
* il est possible de prendre une image sur le site Glénans (ou son Facebook) ;
* choisir une image sous licence libre, type Creative Common (auquel cas mentionner la licence et l’auteur de la photo dans le texte d’accompagnement

Voici des sites recensant des images libre et de qualité:

* [Flickr – images licence libre & intéressantes](https://www.flickr.com/search/?advanced=1&license=2%2C3%2C4%2C5%2C6%2C9&media=photos&sort=interestingness-desc&dimension_search_mode=min&height=1024&width=1024),
* Wikimedia Commons : [Featured pictures](https://commons.wikimedia.org/wiki/Commons:Featured_pictures), ou [Quality images](https://commons.wikimedia.org/wiki/Commons:Quality_images) ou encore [Valued images](https://commons.wikimedia.org/wiki/Commons:Valued_images).
* [500px – Creative Commons](https://500px.com/creativecommons).

## Recommandations sur les photos : la technique

* Redimensionner les photos en 1200 pixel maximum pour éviter les fichiers trop lourd
* Optimiser systématiquement l'image avec le [Kraken](https://kraken.io/web-interface)

## Recommandations sur le texte

* Utiliser les styles de paragraphe pour structure le texte (paragraphe, titre de niveau 1, titre de niveau 2...)
* Ne pas insérer de balise "lire la suite". Il convient de pouvoir lire toute l'actualité d'un coup.

## Recommandations sur les étiquettes (tags)

* Il est recommandé de limiter à quatre (4) le nombre d'étiquettes par actualité. Ceci permettant de choisir les plus pertinentes

Pour information, les catégories ne sont pas utilisées. Toutes les actualités vont dans la catégorie *Actualités*.

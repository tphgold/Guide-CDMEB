---
title: "Rédaction billet Twitter"
weight: 5
---

## Rappel

Twitter n'est utilisé que pour la diffusion d'un article existant sur notre site WWW.moniteurs.glenans.asso.fr (intérêt: avoir une seule et unique source d'information ; corollaire : les statistiques sont plus faciles à consolider)

Dit autrement, nous souhaitons éviter d'avoir un contenu qui n'existerait que sur un réseau social et pas sur notre site

## Rédaction du tweet

* penser à ajouter une illustration (contrairement à FaceBook, l'ajout d'un lien ne génère pas d'image)
* penser à placer une mention à @Les_Glenans
* si possible mettre en cc @CSArzLesGlenans
* ne pas oublier de mettre le lien vers la news :)
---
title: "Créer son compte sur DOC.moniteurs"
date:  2018-01-20T20:04:45+01:00
weight: 5
---

Pour contribuer aux fiches de lecture, il convient d'avoir un compte. Voici les étapes à suivre pour avoir le sien.

1. Depuis le site [DOC.moniteurs.glenans.asso.fr](https://DOC.moniteurs.glenans.asso.fr/), en haut à droite, cliquez sur le lien [Inscription](https://doc.moniteurs.glenans.asso.fr/inscription/).
2. Choisissez un **identifiant** (typiquement pour "Pierre Yves Durand" choisir `pydurand`), et saississez votre **adresse de courriel** (nécessaire pour activer votre compte)
3. Un courriel doit vous parvenir (il faut parfois attendre quelques minutes ; n'oubliez pas de regarder aussi dans vos spams). Dans le message reçu, cliquez sur le lien d'acitivation.
4. Une fois votre compte activé vous recevrez dans la foulée un autre courriel, récapitulant vos informations de connexion.
5. Vous pouvez vous connecter depuis la page [Connexion](https://doc.moniteurs.glenans.asso.fr/wp-login.php)


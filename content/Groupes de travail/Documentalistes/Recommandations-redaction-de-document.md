---
title: "Recommandations rédaction de document"
description: ""
---

Voici quelques recommandations pour la **rédaction** de documents (à placer dans l'espace Documentation des moniteurs)

## Recommandations de rédaction

* Indiquer clairement le nom de l'auteur **dans** le document (par exemple en début ou fin de document).
* Indiquer un moyen de contacter l'auteur. Pour éviter le spam sur les mails, bien penser à ré-écrire le mail de la façon suivante. `jean.dupont@free.fr` doit être écrit `jean.dupont AROBASE free.fr`
* Indiquer la date de rédaction du document (à minima l'année).
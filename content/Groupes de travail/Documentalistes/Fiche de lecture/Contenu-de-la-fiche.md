---
title: "Contenu de la fiche"
weight: 20
---

## 1. Titre (obligatoire)

Le titre de la fiche de lecture, typiquement le titre du document ou mieux, du moment que c'est explicite.

## 2. Description

Une description du document, plus ou moins longue selon l'humeur du rédacteur. Le but de la description est double:

1. Décrire le document de manière factuelle.
2. Donner envie au lecteur de lire le document.

**Note** : le ton employé doit être neutre. L'usage des points d'exclamation est à limiter. L'idée est d'avoir un style similaire à celui du Cours des Glénans (qui ne s'interdit pas un peu d'humour, mais avec modération et retenue).

## 3. Lien de téléchargement (obligatoire)

Après le texte d’accompagnement, il est nécessaire d’ajouter un lien de téléchargement (sinon l'internaute ne le téléchargera jamais le document :) ). Pour mémoire un lien hypertexte est composé de deux éléments :

* l'**intitulé du lien** : typiquement le texte bleu et souligné sur lequel on clique,
* la **cible du lien** : l'adresse web sur laquelle on arrive après avoir cliqué.

### Intitulé du lien

La recommandation pour l'intitulé du lien est d'utiliser la phrase suivante :

> Consulter le Document X (format PDF, 1.3Mo)

...où "Document X" sera remplacé par le nom du document.

### Cible du lien

Maintenant, il convient de récupérer la cible du lien. Pour l’obtenir :

1. Connectez-vous sur le [Drive](https://drive.cdmeb.org/), identifiant `visiteur`, mot de passe `Glenans1947`,
2. Naviguez parmi les dossiers jusqu’au document dont on fait la fiche de lecture.
3. De là, cliquez sur l’icône de partage tout à droite sur la ligne du document (trois points reliés par deux lignes).
4. Cochez *Partager par lien public*,
5. Copiez le lien généré (qui est ledit lien de téléchargement),

Pour finir :

* sélectionnez l'intitulé du lien,
* cliquez sur le bouton "insérer/modifier un lien" (3° icône en partant de la droite, représentant une chaîne),
* collez le lien de téléchargement et validez.

### Exemple

Nous souhaitons créer le lien de partage pour le document "Fiche d'évaluation - Croisière". Pour cela, nous nous connectons au Drive en tant que visiteur, nous nous rendons dans le dossier contenant le document ([Pédagogie > GLENANS Fiches de progression](https://drive.cdmeb.org/index.php/apps/files?dir=/Pedagogie/GLENANS%20Fiches%20de%20progression&fileid=2198)). De là, nous cliquons sur l'icône de partage :

![](/images/Drive_Creer_lien_de_partage_01.png)

Le bandeau de propriétés apparait sur la droite. Pour créer le lien de partage, il suffit de cocher la case **Partager par lien public** :

![](/images/Drive_Creer_lien_de_partage_02.png)

Le lien de partage apparait juste dessous. Pour le copier, il suffit de cliquer sur l'icône à droite du lien.

![](/images/Drive_Creer_lien_de_partage_03.png)

C'est ce lien qu'il faut utiliser comme *cible du lien* dans la fiche de lecture.

## 3. Auteur

Les prénom et nom de l'auteur. Le nom devrait être en MAJUSCULES. Il s'agit bien de l'état civil de l'auteur du document (pas le votre :) )

**Note** : Si l'auteur n'est pas connu, utiliser `Auteur non connu`.

## 4. Courriel de l'auteur

Un moyen de contacter l'auteur, typiquement pour corriger des fautes ou lui faire des suggestions.

Il convient de demander son avis à l'auteur avant d'afficher son courriel. Il est important d'expliquer pourquoi nous affichons le courriel de l'auteur (corrections, enrichissements). Il existe des centaines de documents, mais des documents "vivants" (avec des corrections récentes) sont toujours plus agréables à lire.

**Notes** :

* Afin d’éviter le spam il est important de bien écrire le mail en remplaçant le `@` par ` AROBASE `.  Par exemple pour `jean.dupont@laposte.net` on écrira `jean.dupont AROBASE laposte.net`.
* Si le courriel de l'auteur n'est pas connu, ou si l'auteur refuse d'afficher son courriel, utiliser `courriel non renseigné`.

## 5. Date de rédaction

La date de rédaction, même approximative, du document à télécharger. (Là aussi, il ne s’agit pas de la date de la fiche de lecture mais bien de celle du document). Disposer d’une date sur un document est très important, cela permet au lecteur de positionner le document dans un contexte. Si la date exacte n’est pas connue ce n’est pas grave, par contre il faut trouver a minima l’année de création de document (et saisir comme date le premier janvier de cette année).

**Note** : si la date de rédaction est inconnue ou introuvable, par convention nous utiliserons le 1° janvier 1947.

## 6. Catégorie

La catégorie dans laquelle se range une fiche de lecture.

**Note** : une fiche de lecture ne devrait se trouver que dans une seule catégorie.

## 7. Étiquettes

Les étiquettes existent pour aider à retrouver un ou plusieurs documents. Autant les catégories constituent un classement "rigide", autant les étiquettes sont un classement "souple".

Une fiche de lecture peut avoir de zero (0) à quatre (4) étiquettes maximum. La limitation à quatre permet de choisir les étiquettes les plus pertinentes.

**Notes** :

* Avant de créer une nouvelle étiquette, toujours vérifier qu'une étiquette similaire n'existe pas déjà.
* Les étiquettes ne doivent pas être utilisées pour "remplir plein de mots-clés".

## 8. Image à la une (optionnelle mais fortement recommandée 🙂 )

L’image à la une permet d’ajouter un visuel agréable, elle est optionnelle mais donne un plus non-négligeable à votre fiche de lecture ! Recommandations pour les image à la une :

* **IMPORTANT** Éviter de copier/coller une image prise comme ça sur l’internet (respect du droit d’auteur, nous ne souhaitons pas avoir à gérer d'éventuels contentieux).
* Il est possible de prendre une image sur [l'un des sites Glénans](../../Communication/Liste-des-sites-web.md).
* Il est possible de faire une copie d’écran du document afin de s’en servir comme Image à la une.
* En fin de description de la fiche de lecture, ajouter une mention "Crédit photo : *Photographe X*".
* Voici des sites recensant des images sous Creative Commons et de qualité:
    * [Flickr – images licence libre & intéressantes](https://www.flickr.com/search/?advanced=1&license=2%2C3%2C4%2C5%2C6%2C9&media=photos&sort=interestingness-desc&dimension_search_mode=min&height=1024&width=1024),
    * Wikimedia Commons : [Featured pictures](https://commons.wikimedia.org/wiki/Commons:Featured_pictures),
    * Wikimedia Commons : [Quality images](https://commons.wikimedia.org/wiki/Commons:Quality_images),
    * Wikimedia Commons : [Valued images](https://commons.wikimedia.org/wiki/Commons:Valued_images),
    * [Unsplash](https://unsplash.com/)




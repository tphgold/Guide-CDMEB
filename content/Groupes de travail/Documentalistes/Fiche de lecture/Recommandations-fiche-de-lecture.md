---
title: "Recommandations fiches de lecture"
description: ""
weight: 10
---

## Pré-requis

* Avoir un compte sur DOC.moniteur (voir [Créer son compte sur DOC.moniteurs](/groupes-de-travail/documentalistes/creer-son-compte-sur-doc.moniteurs/)).

## Création de la fiche de lecture

1. S'[identifier sur le site DOC.moniteurs](https://doc.moniteurs.glenans.asso.fr/wp-login.php)
2. Allez dans la section Fiches lecture (en haut à gauche)
3. Cliquez sur le bouton « Add new Fiche Lecture » (en haut)
4. Saisissez votre fiche de lecture en suivant les recommandations ci-dessous.
5. N’oubliez pas de sauvegarder (à intervalle de temps régulier c’est mieux !), avec le bouton « Publier » (à droite)

## Contenu de la fiche de lecture

Les points à ne pas oublier pour le [contenu d'une fiche de lecture](/groupes-de-travail/documentalistes/fiche-de-lecture/contenu-de-la-fiche/)

## Fiches de lecture sur lesquelles prendre exemple

* [Matelotage ABC : Fiche de lecture](https://doc.moniteurs.glenans.asso.fr/fiche-lecture/matelotage-abc/)
* [Matelotage ABC : sur le Drive](https://drive.cdmeb.org/index.php/apps/files?dir=/05_Technologie/Matelotage&fileid=2149)

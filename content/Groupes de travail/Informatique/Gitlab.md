---
title: "Gitlab"
date:  2018-02-10T18:39:12+01:00
weight: 5
---

## Se créer un compte pour Gitlab

1. Se créer un compte sur [Gitlab](https://gitlab.com/users/sign_in#register-pane):
    * *Full name* : le nom complet (exemple : `Jean-Philippe COLAS`)
    * *Username* : l'identifiant (exemple : `jpcolas`)
    * *Email* : votre adresse mail
    * *Password* : le mot de passe que vous choisissez pour Gitlab
2. Suivez les instructions pour activer votre compte
3. Envoyez votre *Username* à `technique AROBASE cdmeb.org` (remplacez `AROBASE` par `@`)

## La suite

Pour bien commencer avec le Gitlab, les liens utiles sont présentés dans la page [Suivi de projet](/gestion-de-projet/suivi-de-projet/).




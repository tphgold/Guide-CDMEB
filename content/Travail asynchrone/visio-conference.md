---
title: "Organisation d'une visio-conférence"
menuTitle: "Visio-conférence"
description: ""
---

Notre [Drive](https://drive.cdmbe.org/) permet de faire un visioconférence, soit entre membre soit avec des personnes extérieures. Pour lancer un visio-conférence :

1. Se connecter au Drive avec ses identifiants
1. Cliquer sur l'icône caméra ("appel vidéo") en haut à gauche de l'écran
1. Soit choisir les noms des personnes à inviter, soit choisir "appel public" et envoyer le lien par courriel aux participants

* qualité du service : à tester
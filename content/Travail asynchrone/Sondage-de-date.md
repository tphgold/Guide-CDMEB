---
title: "Organisation d'un sondage de date"
menuTitle: "Sondage de date"
description: ""
---

Un sondage de date permet aux membres d'un groupe de travail de décider d'une date de réunion.

Nous utilisons [Framadate](http://framadate.org/), porté par une association d'éducation populaire (Framasoft) et s'appuyant sur un logiciel libre. Il est préférable d'utiliser [Framadate](http://framadate.org/) plutôt que Doodle qui récolte les données personnelles (dont le courriel) et peu respectueux de la vie privée.

De plus, Framadate offre une option inexistante chez Doodle en plus du oui/non : le "s'il le faut je me libère", qui s'avère très pratique !

* qualité du service : excellent

## Recommandations

Les points suivants sont des bonnes pratiques et permettent au sondage de bien se dérouler.

* Proposer une plage de date relativement étendue (typiquement sur deux semaines) ; sinon les chances de devoir refaire un nouveau sondage augmentent, et ça agace tout le monde :)
* Voter (donner ses disponibilités) rapidement dès la réception du courriel ; sinon les disponibilités des personnes changent et il faut refaire un sondage. Idéalement, l'organisateur demande aux participants de voter avant un date donnée ; ceci permet à chacun de s'organiser pour voter de manière asynchrone.
* Donner ses disponibilités **indépendamment** de celles des autres ; sinon ce n'est plus un sondage :)
* Donner ses disponibilités indépendamment de celles des autres, *même si on est le dernier à voter* ; ceci aide l'organisateur à faire des choix en cas de plannings incompatibles
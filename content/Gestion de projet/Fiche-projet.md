---
title: "Fiche Projet"
date:  2018-03-09T15:35:43+01:00
weight: 5
---


# Qu'est-ce qu'un projet ?

Pour devenir un projet, une idée doit avoir a minima :

* un titre
* une description
* un référent

Autres points optionnels mais importants

* une échéance
